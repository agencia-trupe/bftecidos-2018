// enviando contato
head.js(JS_PATH+'/is.simple.validate.js',function(){
    $('#contato-form').submit(function(e){
        e.preventDefault();
        var $form = $(this),
            $status = $('.response p').removeClass('error').html('');
        $status.html(TRANSLATE.campo_status_enviando);
        
        if($form.isSimpleValidate()){
        //if(validadeForm($form)){
            var url  = URL+'/'+CONTROLLER+'/enviar.json',
                data = $form.serialize();
            
            $.post(url,data,function(json){
                if(json.error){
                    // $status.addClass('error').html('* '+json.error);
                    alert('* '+json.error);
                    return false;
                }
                
                // $form.fadeOut("slow", function() {
                    $status.html(json.msg).parent().fadeIn();
                // });
            },'json');
        } else {
            // $status.addClass('error').html('* '+TRANSLATE.campo_status_preencha);
            alert('* '+TRANSLATE.campo_status_preencha);
        }
        
        return false;
    });
    
    $('.response').click(function(){
        $(this).fadeOut(function(){
            resetForm('form#contato-form');//.fadeIn();
        });
    });
});

function resetForm (sel) {
    $form = $(sel);
    $form.find('input,textarea').val('');
    $form.find('select option').attr('selected',false);

    return $form;
}

$('.representantes-opener').click(function(e){
    var $this = $(this);
    
    if($this.hasClass('closed')) $('.representantes').slideDown(function(){
        $this.removeClass('closed');
    }); else $('.representantes').slideUp(function(){
        $this.addClass('closed');
    });
    
});