$('#play-video').click(function(e){
	e.preventDefault();
	
	if($(this).data('has-video')){
		$('#video iframe').attr('src',$(this).attr('href'));
		$('#video').show();
	}
	
	return false;
});