(function ($) {
    $.fn.isSimpleTabs = function(){
        var tabContainers = $(this).find('div.tabs-container'),
            hasHash = (window.location.hash && $('div.tabs ul.tabs-navigation a[href="'+window.location.hash+'"]').size())?
                        window.location.hash : false;
        //console.log(tabContainers);return;
        
        $('div.tabs ul.tabs-navigation a').click(function () {
            tabContainers.hide().filter(this.hash).show();
            
            $('div.tabs ul.tabs-navigation a').removeClass('selected');
            $(this).addClass('selected');
            
            return false;
        }).filter((hasHash)?'[href="'+hasHash+'"]':':first').click();
    }
})(jQuery);