(function($){
	$('#blog-busca').submit(function(e){
		e.preventDefault();
		window.location = URL+'/blog/busca/'+$(this).find('input[name=busca]').val();
		return false;
	});

	$('#blog-comentario').submit(function(e){
		e.preventDefault();

		var form   = $(this),
			status = form.find('.status').removeClass('error').html('').show(),
			url    = [URL,CONTROLLER,'comentar.json'].join('/');

		form.find('.submit').attr('disabled',true);
		status.html(TRANSLATE.campo_status_enviand);//alert(url);return;

		$.post(url,form.serialize(),function(json){
			form.find('.submit').attr('disabled',false);

			if(json.error){
				// status.addClass('error').html(json.msg).click(function(){$(this).fadeOut();});
				status.html('');
				alert(json.msg);
				return false;
			}

			status.html(json.msg).delay(5000).fadeOut();

			var date = new Date(),
				dt   = date.getHours()+'h'+date.getMinutes(),
				user = ((typeof(USER)!='undefined') ? 
							USER.nome : 
								($.trim($('#nome').val()) != '' && $.trim($('#nome').val()) != 'Nome') ?
									$.trim($('#nome').val()) : TRANSLATE.anonimo),
				html = '<div class="comentario">'+
					   '<p class="info">'+user+' · '+dt+'</p>'+
					   '<p>'+
					   nl2br(form.find('#comentario').val(),false)+
					   '</p>'+
					   '</div>';

			$(html).hide().appendTo('#comentarios').slideDown('slow');
			$('#comentario,#nome,#email').val('');
		},'json');

		return false;
	});
})(jQuery);

// nl2br
function nl2br(str,is_xhtml){var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '' : '<br>';return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');}