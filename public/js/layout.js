var fbLoaded, cycleLoaded, hammerLoaded, 
    windowWidth, isDesktop, isMobile, isTablet, isSmartphone;

(function($){
    if(APPLICATION_ENV == 'development') head.js(JS_PATH+'/less.js',function(){
        // less.watch();
        if(1) intLess = window.setInterval(function(){
            localStorage.clear();
            less.refresh();
        },1500*2);
    }); // load less in dev

    // máscaras padrões
    $("input.mask-cpf").mask("999.999.999-99");
    $("input.mask-cep").mask("99999-999");
    $("input.mask-data,input.mask-date").mask("99/99/9999");
    $("input.mask-tel").mask("(99)9999-9999");
    $("input.mask-cel").mask("(99)9?9999-9999");
    $("input.mask-currency").maskCurrency();
    _onlyNumbers('.mask-int');

    $(".prevalue").each(function(){ $(this).prevalue(); }); // prevalue em campos com a class .prevalue

    $("a[href^='#']").live("click",function(e){ // remove click de links vazios (que começam por #)
        e.preventDefault();
    });
    $("a.js-back").live("click",function(e){ // adiciona ação de voltar a links com a classe js-back
        history.back();
    });

    // ativa menus
    var menus_ativar = {
        '/colecao/' : 5,
        '/colecoes/' : 5,
        '/projeto/' : 4,
        '/blog/' : 6
    };

    for(i in menus_ativar){
        if(location.href.indexOf(i)!=-1){
            var $li = $('#top nav ul li').eq(menus_ativar[i]);
            $li.addClass('active').find('a').addClass('active');
        }
    }

    $('#mobile-menu').click(function(e){
        $(this).toggleClass('active');
        $('#top-menu .navigation').toggleClass('show');
    });

    checkMobile();
    $(window).resize(function(e){
        checkMobile();
    });

    if($(".pagination").size()){ // se tiver paginação
        $(".pagination .navigation.left").html("&laquo;"); // arruma texto da navegação
        $(".pagination .navigation.right").html("&raquo;");
        if($(".pagination a").size() <= 1){
            $(".pagination a").hide(); // se tiver somente 1 ou nenhuma página esconde a div
        }
    }

    // flash-messages
    if($("#flash-messages").size()){
        var $flash = $("#flash-messages");
        var y = (document.documentElement.scrollTop) ? document.documentElement.scrollTop : document.body.scrollTop;
        $flash.css({top:y+10}).delay(1000).show().click(function(){
            $(this).fadeOut("slow");
        });
    }

    // scroll to
    var $scrollTos = $('a.scroll-to');
    if($scrollTos.length){
        head.js(JS_PATH+'/jquery.scrollTo-min.js',function(){
            $scrollTos.live('click',function(e){
                var $this = $(this);
                $.scrollTo($($this.attr('href')),1000,{axis:'y'});
            });
        });
    }

    // img preload
    var $preloadImgs = $('[data-preload-img]');
    if($preloadImgs.length){
        $preloadImgs.each(function(){
            _preloadImg($(this).data('preload-img'));
        });
    }

    if(SCRIPT) head.js(JS_PATH+'/'+SCRIPT+'.js'); // carrega scripts de páginas

    // controller specific
    switch(CONTROLLER){
        case 'index':
            _banners();
            break; 

        case 'projeto':
            $projeto_imgs = $('.projeto img');
            
            if($projeto_imgs.length){
                $projeto_imgs.each(function(){
                    var $this = $(this), a = '';

                    a+= '<a ';
                    a+= 'href="'+$this.attr('src').replace(URL+'/img/projetos|',IMG_PATH+'/projetos/')+'" ';
                    // a+= 'title="'+$this.data('descricao')+'" ';
                    a+= 'rel="projeto_group" ';
                    a+= 'class="fb-image" ';
                    a+= '></a>';

                    $this.wrap(a);
                });

                _loadFancybox(function(){
                    $('a[rel=projeto_group]').fancybox({
                        'titlePosition':'inside'
                    });
                });
            }
            break;

        case 'blog':
            $('#arquivo').change(function(){
                $('#arquivos ul li a').hide();
                $('.arquivo-'+$(this).val()).show();
            });
            break;

        case 'marcas':
            var _fbOptsMarcas = {
                'padding':0,
                // 'titlePosition':'inside',
                'titleShow':false
            };
            if(isDesktop) {
                _fbOptsMarcas['width'] = 850;
                if($.browser.msie) _fbOptsMarcas['width'] = 890;
                if($.browser.mozilla) _fbOptsMarcas['width'] = 890;
                _fbOptsMarcas['height'] = 610;
                _fbOptsMarcas['autoScale'] = false;
                _fbOptsMarcas['autoDimensions'] = false;
            } else if(isTablet) {
                _fbOptsMarcas['width'] = 640;
                _fbOptsMarcas['height'] = 610;
                _fbOptsMarcas['autoScale'] = false;
                _fbOptsMarcas['autoDimensions'] = false;
            }

            _loadFancybox(function(){
                $('a.marca').fancybox(_fbOptsMarcas);
            });
            break;

        case 'colecao':
            // menu lateral
            $('.navigation > li > a.item').click(function(e){
                var $this = $(this),
                    $menu = $this.next('.navigation-sub'),
                    $menus = $('.navigation-sub');
                
                if($this.hasClass('active')) {
                    $menu.slideUp(function(){
                        $this.removeClass('active');
                    });
                } else {
                    $menus.slideUp(function(){

                    });
                    $('.navigation > li > a.item').removeClass('active');
                    $menu.slideDown(function(){
                    
                    });
                    $this.addClass('active');
                }
            });

            // imagens galeria
            // antigo, tremia imagens na transição (chrome:win)
            $('#colecoes-list .colecao1').live('click',function(e){
                e.preventDefault();

                var $this = $(this), toi,
                    $imgc = $('.img-0-container'),
                    $imgs = $('.img-0'), 
                    $img  = $('.img-0 .img'), 
                    $img2 = $('.img-0 .bg'), 
                    $next = $this.next();

                $img2.attr('src',$this.attr('href'));
                $img.css('opacity','0');
                // $imgc.css('background-image','url('+$this.attr('href')+')');
                // $imgs.css('background-image','url('+$this.attr('href')+')').css('opacity','1');

                // $.get($this.attr('href'),function(){
                    // $imgs.css('background-image','url('+$this.attr('href')+')');
                    window.setTimeout(function(){
                        $img.attr('src',$this.attr('href'));
                        window.setTimeout(function(){
                            $img.css('opacity','1');
                        },100);
                    },500);
                // });

                if(e.which) {
                    autoplayColecao = 0;
                    cntCols = $('#colecoes-list .colecao').index($this) + 1;
                    window.setTimeout(function(){autoplayColecao=1},timeoutCols);
                }
                return false;
            });
            
            var cntBgSw = -1;
            $('#colecoes-list .colecao').live('click',function(e){
                e.preventDefault();
                cntBgSw++;

                var $this = $(this), toi,
                    $imgc = $('.img-0-container'),
                    $imgs = $('.img-0'), 
                    $img  = $('.img-0 .img'), 
                    $img2 = $('.img-0 .bg'), 
                    $next = $this.next();

                if(cntBgSw%2==0){
                    $img2.css({'opacity':'1'});
                    $img2.attr('src',$this.attr('href'));
                } else {
                    $img.css({'opacity':'1'});
                    $img.attr('src',$this.attr('href'));
                }
                
                window.setTimeout(function(){
                    if(cntBgSw%2==0){
                        $img.css({'opacity':'0'});
                    } else {
                        $img2.css({'opacity':'0'});
                    }
                },600);

                if(e.which) {
                    autoplayColecao = 0;
                    cntCols = $('#colecoes-list .colecao').index($this) + 1;
                    window.setTimeout(function(){autoplayColecao=1},timeoutCols);
                }
                return false;
            });
            
            // preload de imagens
            $('#colecoes-list .colecao').each(function(i,elm){
                var newImg = new Image();
                newImg.src = elm.href;
            });
            // autoplay de imagens
            var $cols = $('#colecoes-list .colecao'), col, $col,
                cntCols = 1,
                cntColsMax = $cols.size(),
                timeoutCols = 5000,
                autoplayColecao = 1;
            window.setInterval(function(){
                if(!autoplayColecao) return;
                $($cols[cntCols]).trigger('click');
                cntCols++;
                if(cntCols > cntColsMax) cntCols = 0;
            },timeoutCols);
            break;
    }
})(jQuery);

// --------------------------------- GENERAL FUNCTIONS --------------------------------- >

// Color Hex <-> RGB
function hexToR(h){return parseInt((cutHex(h)).substring(0,2),16)}
function hexToG(h){return parseInt((cutHex(h)).substring(2,4),16)}
function hexToB(h){return parseInt((cutHex(h)).substring(4,6),16)}
function cutHex(h){return (h.charAt(0)=="#") ? h.substring(1,7):h}
function hexToRGB(h){return {'R':hexToR(h),'G':hexToG(h),'B':hexToB(h)}}
function RGBToHex(r,g,b){var dec = r+256*g+65536*b;return dec.toString(16);}

// Math
function getRand(n){n=n||0;return Math.floor(Math.random()*(n+1));}

// ValidateForm
function validadeForm(f){var $f=$(f),err=0;$f.find("input").each(function(i,v){if($(this).data("validate")&&$.trim($(this).val())==""){alert($(this).data("errmsg"));$(this).focus();err++;return false}});if(err==0){$f.submit()}};

// onlyNumbers
function _onlyNumbers(selector){
    $(selector).keypress(function(e){
        return (e.which >= 48 && e.which <= 57) || ($.inArray(e.which,[0,13,8]) != -1);
    });
}

function _preloadImg(src){
    var img = new Image();
    img.src = src;
}

function _log(log){ return typeof(window.console)=='undefined' ? alert(log) : console.log(log); }
function _d(log){ return _log(log); }

function _banners(){
    _loadCycle(function(){
        $('#banners').cycle({fx:'fade',speed:3000});
    });

    var $video = $('#videos a');

    if($video.attr('href')!='#'){
        _loadFancybox(function(){
            $video.fancybox({
                type: 'iframe'
            });
        });
    }
}

function _loadFancybox(callback){
    if(fbLoaded){
        if(typeof(callback)=='function') (callback)();
    } else {
        $.getCss(CSS_PATH+'/fancybox/jquery.fancybox-1.3.4.css');
        head.js(JS_PATH+'/jquery.fancybox-1.3.4.pack.js',function(){
            fbLoaded = true;
            if(typeof(callback)=='function') (callback)();
        });
    }
}

function _loadCycle(callback){
    if(cycleLoaded){
        if(typeof(callback)=='function') (callback)();
    } else {
        head.js(JS_PATH+'/cycle.min.js',function(){
            cycleLoaded = true;
            if(typeof(callback)=='function') (callback)();
        });
    }
}

function _loadHammer(callback){
    if(hammerLoaded){
        if(typeof(callback)=='function') (callback)();
    } else {
        head.js(JS_PATH+'/hammer.min.js',function(){
            hammerLoaded = true;
            if(typeof(callback)=='function') (callback)();
        });
    }
}

// mobile
function checkMobile(){
    windowWidth = $(window).width(); // window.innerWidth
    isDesktop = windowWidth >= 1130;
    isMobile = windowWidth < 1130;
    isTablet = windowWidth < 1130 && windowWidth >= 680;
    isSmartphone = windowWidth < 680;

    var $html = $('html');

    var log = windowWidth+
              (isDesktop ? ':Desktop' : '')+
              (isMobile ? ':Mobile' : '')+
              (isTablet ? ':Tablet' : '')+
              (isSmartphone ? ':Smartphone' : '');

    if(CONTROLLER=='colecoes' || CONTROLLER=='colecao') {
        if($('#colecoes-list').hasClass('cycled')) {
            $('#colecoes-list').cycle('destroy');
            $('#colecoes-list').removeClass('cycled');
        }

        var fmax = CONTROLLER=='colecoes' ? 4 : 6, cycleOptsCol;
        
        if(!isSmartphone) {
            if($('#colecoes-list .colecao').size()>fmax) {
                // verificando se há <li> vazio
                var $liClc = $('#colecoes-list li');
                if($liClc.length > $liClc.has('a').length)
                    $liClc.filter(':last-child').remove();
                
                _loadCycle(function(){
                    cycleOptsCol = {
                        fx:'fade',
                        // timeout: 0,
                        prev:'#prev',
                        next:'#next',
                        pager:'#colecoes-pager',
                        speed:'fast'
                    };
                    if(CONTROLLER=='colecoes') {
                        // cycleOptsCol.timeout = 5000;
                        cycleOptsCol.speed = 1000;
                    } else {
                        cycleOptsCol.timeout = 0;
                    }
                    // window.setTimeout(function(){ // test pre-cycled
                    $('#colecoes-list').removeClass('pre-cycled');
                    $('#colecoes-list').cycle(cycleOptsCol);
                    $('#colecoes-list').addClass('cycled');
                    // },3000);
                });
            }
        } else {
            $('#colecoes-list').removeClass('pre-cycled');
        }
    }

    if(CONTROLLER=='colecao' && isSmartphone) {
        var $links = $('#colecoes-list .colecao');
        console.log($links);
        $('#colecoes-list').html('').append('<li>').append($links);
        $('#colecoes-list li').append($links);
    }

    if(isMobile){
        // ações de toque (touch)
        _loadHammer(function(){
            if(CONTROLLER=='index' && ACTION=='index'){
                // next/prev slideshow home
                Hammer($('#slideshow-wrapper').get(0)).on('swipeleft',function(e){
                    $("#slideshow").cycle('next');
                }).on('swiperight',function(e){
                    $("#slideshow").cycle('prev');
                });
            }
        });
    }
    if(isTablet){
        // ações de toque (touch)
        _loadHammer(function(){
            if(CONTROLLER=='colecoes'||CONTROLLER=='colecao'){
                // next/prev slideshow home
                Hammer($('#colecoes-list').get(0)).on('swipeleft',function(e){
                    $("#colecoes-list").cycle('next');
                }).on('swiperight',function(e){
                    $("#colecoes-list").cycle('prev');
                });
            }
        });
    }
    if(isSmartphone){ }
    if(isDesktop){ }

    if(APP_ENV=='development') console.log(log);
}