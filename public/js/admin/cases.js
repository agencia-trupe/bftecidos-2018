var MAX_FOTOS = 0, MAX_SIZE = 5242880, allow_photos=false;

$(document).ready(function(){
    if(document.getElementById('paginas_fotos2')){
        $('input.foto_descricao').each(function(i,v){
            var $t = $(this);
            if($.trim($t.val())=='') $t.prevalue('Descrição...');
        });
        
        $('input.foto_descricao').live('keyup',function(e){
            if(e.which==13) FotoDesc.save($(this));
        });
    }
});

var FotoDesc = {
    save : function(elm){
        if($.trim(elm.val())==''){
            //alert('Preencha o campo descrição.');
            //elm.focus();
            //return false;
        }
        
        var url  = GLOBAL_URL+'save-descricao.json',
            data = {
                'pagina_id' : elm.data('pagina-id'),
                'foto_id'   : elm.data('foto-id'),
                'descricao' : elm.val()
            },
            status = elm.parents('li').find('.foto_status');
        
        elm.attr('disabled',true);
        status.html('Aguarde...').show();
        
        $.post(url,data,function(json){
            elm.attr('disabled',false);
            
            if(json.error){
                alert(json.error);
                status.html('');
                return false;
            }
            
            status.html(json.msg||'Concluído').delay(3000).fadeOut('slow');
        },'json');
    }
}