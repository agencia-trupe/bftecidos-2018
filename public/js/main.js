! function(e) {
    e(document).ready(function() {
        function o() {
            e(window).height() < 350 ? e("footer").addClass("mobile-horz") : e("footer").removeClass("mobile-horz")
        }

        if(CONTROLLER=='index') slideShowHome();

        var a = e(".projetos-categorias a.active");
        e(".projetos-categorias a").hover(function() {
            a.css("border-color", "transparent")
        }, function() {
            a.css("border-color", "#fff")
        });
        var i = e(".midias-categorias a.active");
        e(".midias-categorias a").hover(function() {
            i.css("color", "#fff")
        }, function() {
            i.css("color", "#5c4f41")
        }), e(".lightbox,.clipping-thumb").fancybox({
            padding: 0,
            maxWidth: "90%",
            fitToView: !1
        }), e("#contato-form1").submit(function(o) {
            o.preventDefault(), e(this).fadeOut("slow", function() {
                e(".response").fadeIn()
            })
        }), e(".hamburger").click(function(o) {
            o.preventDefault(), e(".mobile-menu-wrapper").fadeIn("fast")
        }), e(".mobile-menu .fechar").click(function(o) {
            o.preventDefault(), e(".mobile-menu-wrapper").fadeOut("fast")
        }), e(window).resize(o).trigger("resize")
    })
}(jQuery);

function slideShowHome() {
    var imgs = [];
    $('#slideshow .slide').each(function(i,elm){
        var img = new Image();
        img.src = getBgImg(elm);
        img.onload = function() { $(this).data('loaded',true); };
        if(i==0) $(elm).css('background-image','url('+img.src+')');
        imgs.push(img);
    });

    window._homeInt = window.setInterval(function(){
        var loaded = true;
        for(var i in imgs) if(!Boolean($(imgs[i]).data('loaded'))) loaded = false;
        if(!loaded) return false;

        $('#slideshow .slide').each(function(i,elm){
            var im = imgs[i].src;
            if(im) $(elm).css('background-image','url('+im+')');
            else console.log(im);
        });

        console.log('loaded');
        window.clearInterval(window._homeInt);
        $("#slideshow").cycle({
            slideResize: true,
            containerResize: true,
            width: '100%',
            height: '100%',
            fit: 1,
            
            fx: "fade",
            speed: "slow",
            slides: "> div",
            pager: "#slideshow-pager",
            pagerTemplate: '<a href="#">{{slideNum}}</a>'
        });
    },500);
}

function getBgImg(elm) {
    return $(elm).data('bg');
    // return $(elm).css('background-image').replace('url(','').replace(')','');
}