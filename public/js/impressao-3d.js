var Shop;

$.getCss(JS_PATH+'/fileinput/fileinput.css');
head.js(JS_PATH+'/is.shop.js',JS_PATH+'/fileinput/jquery-ui-1.8.9.custom.min.js',JS_PATH+'/fileinput/jquery.fileinput.min.js',function(){
	Shop = $.isShop;
	Shop.init();

	$('#fazer-impressao').click(function(){
		var $this = $(this);
		Shop.showFormImpressao3d($('#shop-content').html());
		// $("input.mask-tel").mask("(99)9999-9999");
		_onlyNumbers('.mask-int');

	    $('.shop-message-box #arquivo').fileinput({
	        inputText: 'Nenhum arquivo selecionado',
	        buttonText: 'Selecionar arquivo'
	    });
	    $('.shop-message-box #arquivo').css({'left':0,'top':'10px'});
	});

	if($('#shop-content').hasClass('retorno')){
		Shop.showFormImpressao3d($('#shop-content').html());
	}
});

/*// campo upload
$.getCss(JS_PATH+'/fileinput/fileinput.css');
head.js(JS_PATH+'/fileinput/jquery-ui-1.8.9.custom.min.js',JS_PATH+'/fileinput/jquery.fileinput.min.js',function(){
    $('#arquivo').fileinput({
        inputText: 'Nenhum arquivo selecionado',
        buttonText: 'Selecionar arquivo'
    });
    $('#arquivo').css({'left':0,'top':'10px'});
});*/