(function($){
	if(ACTION=='materias'){
		$('.materia-open').click(function(e){
			e.preventDefault();
			var $this = $(this);

			if($this.hasClass('materia-open')){
				$this.find('.descricao-completa').slideDown();
				$this.find('.descricao').hide();
				$this.find('.materia-completa').hide();
			}

			if($this.hasClass('materia-close')){
				$this.find('.descricao-completa').slideUp();
				$this.find('.descricao').show();
				$this.find('.materia-completa').show();
			}

			$this.toggleClass('materia-open').toggleClass('materia-close');

			return false;
		});
	}
})(jQuery);