<?php

class Application_Form_FacaParte extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')
             ->setAction(URL.'/contato/faca-parte')
             ->setAttrib('id','frm-fale-conosco')
             ->setAttrib('name','frm-fale-conosco');
        
        // elementos
		$this->addElement('text','nome',array('label'=>'nome','class'=>'txt2'));
        $this->addElement('text','email',array('label'=>'e-mail','class'=>'txt2'));
        // $this->addElement('hidden','vaga_id',array('label'=>'cargo pretendido','class'=>'txt2'));
        // $this->addElement('hidden','horario',array('label'=>'horário','class'=>'txt2'));
        $this->addElement('text','tel',array('label'=>'telefone','class'=>'txt2 mask-tel'));
        $this->addElement('text','portfolio',array('label'=>'portfolio','class'=>'txt2'));
        $this->addElement('text','mensagem',array('label'=>'mensagem','class'=>'txt2'));
        
        // filtros / validações
        $this->getElement('nome')->setRequired()
             ->addFilter('StripTags')
             ->addFilter('StringTrim');
        $this->getElement('tel')->setRequired()
             ->addFilter('StripTags')
             ->addFilter('StringTrim');
        /*$this->getElement('vaga_id')->setRequired()
             ->addFilter('StripTags')
             ->addFilter('StringTrim');
        $this->getElement('horario')->setRequired()
             ->addFilter('StripTags')
             ->addFilter('StringTrim');*/
        $this->getElement('email')->setRequired()
             ->addValidator('EmailAddress')
             ->addFilter('StripTags');
        
        // remove decoradores
        $this->removeDecs();
    }
}

