<?php

class Admin_HomeController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Application_Model_Login::checkAuth($this);

        $this->view->titulo = "HOME";
        $this->view->section = $this->section = "home";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/".$this->section;
        
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
        $this->form = new Admin_Form_Home();
        $this->pagina_home = new Application_Model_Db_PaginaHome();
        $this->pagina_home_fotos = new Application_Model_Db_PaginaHomeFotos();
        
        $this->view->MAX_FOTOS = 15;
        $this->view->MAX_SIZE = intval(ini_get('post_max_size'));

        // Admin_Model_Login::checkAuth($this,$this->section) ||
            // $this->_forward('denied','error','default',array('url'=>URL.'/admin'));
    }

    public function indexAction()
    {
        $form = $this->form;
        $row = $this->pagina_home->fetchRow('id=1');
        $this->view->id = $this->produto_id = 1;
        
        if($row) $form->populate(array(
            // 'titulo_pt' => utf8_encode($row->titulo_pt),
            // 'titulo_en' => utf8_encode($row->titulo_en),
            'texto1_pt' => utf8_encode($row->texto1_pt),
            'texto1_en' => utf8_encode($row->texto1_en),
            // 'texto2_pt' => utf8_encode($row->texto2_pt)
            // 'texto2_en' => utf8_encode($row->texto2_en)
            // 'video' => utf8_encode($row->video),
            'video_pt' => utf8_encode($row->video_pt),
            'video_en' => utf8_encode($row->video_en),
        ));
        
        $this->view->form = $form;
        $this->view->fotos = $this->fotosAction(1);
        // $this->view->fotos2 = $this->fotosAction(2);
    }
    
    public function saveAction()
    {
        $id = 1;
        $params = $this->_request->getParams();
        $row = $this->pagina_home->fetchRow('id='.(int)$id);
        
        try {
            $data = array();
            // $data['video']    = utf8_decode($params['video']);
            $data['video_pt']    = utf8_decode($params['video_pt']);
            $data['video_en']    = utf8_decode($params['video_en']);
            // $data['titulo']    = utf8_decode($params['titulo']);
            $data['texto1_pt']    = utf8_decode((cleanHtml($params['texto1_pt'],'<b><a><i><u><br><ul><ol><li><img><h3>')));
            $data['texto1_en']    = utf8_decode((cleanHtml($params['texto1_en'],'<b><a><i><u><br><ul><ol><li><img><h3>')));
            // $data['texto2']    = utf8_decode($params['texto2']);
            $data['user_edit'] = $this->login->user->id;
            $data['data_edit'] = date("Y-m-d H:i:s");
            
            if($row){
                $this->pagina_home->update($data,'id = '.(int)$id);
            } else {
                $data['user_cad'] = $this->login->user->id;
                $data['data_cad'] = date("Y-m-d H:i:s");
                $id = $this->pagina_home->insert($data);
            }
            
            //return array("id"=>$id);
            $this->messenger->addMessage('Página atualizada.');
            $this->_redirect('admin/'.$this->section);
        } catch(Exception $e) {
            $this->messenger->addMessage($e->getMessage(),'error');
            $this->_redirect('admin/'.$this->section);
        }
    }
    
    public function fotosAction($tipo=1)
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('pagina_home_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->where('f2.tipo = '.$tipo)
            ->order('f.ordem asc');
        
        if(isset($this->produto_id)){
            $select->where('f2.pagina_id = ?',$this->produto_id);
        }
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            $fotos->delete("id=".(int)$id);
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '512M');
        $max_size = intval(ini_get('post_max_size')).'MB'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/'.$this->section));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            // $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            // $thumb->resize('1000','1000');
            // $thumb->save($this->img_path.'/'.$rename);
            // Image::process($this->img_path.'/'.$rename);
            // Image::process($this->img_path.'/'.$rename,array('quality'=>70));
            
            $fotos = new Application_Model_Db_Fotos();
            $produtos_fotos = new Application_Model_Db_PaginaHomeFotos();
            $produto_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $produtos_fotos->insert(array(
                "foto_id"=>$foto_id,
                "pagina_id"=>$produto_id,
                "tipo"=>($this->_hasParam('t')?$this->_getParam('t'):1)
            ));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }

    public function saveAllAction()
    {
        if(!$this->_hasParam('id')) {
            return array('error'=>'Acesso negado');
        }
        
        $f = new Application_Model_Db_Fotos();
        $post = $this->_request->getParams();
        $id = $post['id'];

        // limpando dados
        $limpar = array('module','controller','action','portfolio_id','id');
        foreach($limpar as $l) if(isset($post[$l])) unset($post[$l]);
        foreach($post as $k=>$v){
            $post[$k] = utf8_decode($v);
            if($v=='__none__') $post[$k] = null;
        }
        
        if(empty($post)) return array('error'=>'Preencha os campos');

        try{
            $f->update($post,'id='.$id);
            
            return array('msg'=>'Salvo.');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }

    /**
     * Salva ordenação via ajax
     */
    public function ordemFotoAction()
    {
        $this->fotos = new Application_Model_Db_Fotos();
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            for($i=0;$i<sizeof($values['id']);$i++) $this->fotos->update(array(
                'ordem' => $i+1 //$values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            $err = 'Erro ao salvar ordenação';
            if(APPLICATION_ENV!='production') $err.= ': '.$e->getMessage();
            return array('error'=>$err);
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}

