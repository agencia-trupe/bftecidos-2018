<?php

class Admin_CasesController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "CASES";
        $this->view->section = $this->section = "cases";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
        $this->cases = new Application_Model_Db_Cases();
        
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/..".FILE_PATH."/".$this->section;
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 20;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%'";
            $rows = $this->cases->fetchAll($where,$post['search-by'],$limit,$offset);
            
            $total = $this->view->total = $this->cases->count($where);
        } else {
            $rows = $this->cases->fetchAll(null,"data_cad desc",$limit,$offset);
            $total = $this->view->total = $this->cases->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; NOVO");
        
        if($this->_hasParam('data')){ // se possui dados
            $data = $this->_getParam('data');
            $this->view->id = $this->pagina_id = $data['id'];
			
			$this->view->allow_photos = $data['allow_photos'];
			$this->view->allow_photos2 = true;
			$this->view->allow_files  = $data['allow_files'];
			$this->view->alias        = $data['alias'];
            
			$form = new Admin_Form_Cases($data['id']);
            $form->addElement('hidden','id');
            $this->fotosAction();
			$this->view->action = 'edit';
        } else {
			$form = new Admin_Form_Cases();
            $data = array('status_id'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){ // se ñ for post gera erro
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/cases/new'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->cases->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $post = $this->_request->getParams();
            $data = array_map('utf8_decode',$post);
            $data['alias']       = Is_Str::toUrl($post['titulo'].((bool)$post['cod'] ? ' '.$post['cod'] : ''));
            $data['body']   = strip_tags($data['body'],'<b><strong><a><i><u><br><ul><ol><li><img><h1><h2><h3>');
			$data['user_edit']   = $this->login->user->id;
            $data['data_edit']   = date("Y-m-d H:i:s");
            $data['parent_id']   = $data['parent_id']=='__none__' ?
                                    0 :
                                    $data['parent_id'];
            
            if(!$row){ // adiciona dados padrão
                $data['user_cad']  = $this->login->user->id;
                $data['data_cad']  = date("Y-m-d H:i:s");
                $data['allow_photos'] = 1;
                $data['allow_files'] = 1;
            }
            
            // checa se já possui filhos antes de adicionar um pai
            if($row && $data['parent_id']!=0){
                if($this->cases->hasChildren($row->id)){
                    $this->messenger->addMessage('Este projeto não pode ter um projeto pai.','error');
                    $this->_redirect('admin/'.$this->section.'/edit/'.$id);
                }
            }
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            // atualiza
            ($row) ? $this->cases->update($data,'id='.$id) : $id = $this->cases->insert($data);
            
            // redireciona
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $data = Is_Array::utf8All($data);
            $this->_redirect('admin/'.$this->section.'/edit/'.$id);
            //$this->_forward('new',null,null,array('data'=>$data));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ?
                     'Já existe um projeto com este nome, escolha um novo.' :
                     $e->getMessage();
            
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->cases->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('denied','error','default',array('url'=>URL.'/admin/cases'));return false; }
        
        $data = Is_Array::utf8All($row->toArray());
        //Is_Var::dump($data);
        $this->_forward('new',null,null,array('data'=>$data));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        $table = new Application_Model_Db_Cases();
        
        try {
            $table->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function edit1Action()
    {
        $alias = $this->_getParam('alias');
        $table = $this->cases;
        $row   = $table->fetchRow('alias = "'.$alias.'"');
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>URL.'/admin/'));return false; }
        
        $parent= $table->fetchRow('id = '.(int)$row->parent_id);
        $form  = new Admin_Form_Cases($row->id);
        
        $this->view->id           = $this->pagina_id = $row->id;
        $this->view->allow_photos = $row->allow_photos;
        $this->view->allow_photos2 = true;
        $this->view->allow_files  = $row->allow_files;
        $this->view->alias        = $row->alias;
        $this->view->titulo = $parent ?
                              Is_Str::toUpper(utf8_encode($parent->titulo))." &rarr; ".Is_Str::toUpper(utf8_encode($row->titulo)):
                              $this->view->titulo." &rarr; ".Is_Str::toUpper(utf8_encode($row->titulo));
        
        if($this->_request->isPost()){
            $post = $this->_request->getPost();
            
            if(!$form->isValid($post)){
                $this->messenger->addMessage("Preencha todos os campos corretamente.","erro");
            } else {
                try {
                    $row->body = utf8_decode(Is_Str::nl2none(strip_tags($post['body'],'<b><strong><a><i><u><br><ul><ol><li><img><h1><h2><h3>')));
                    //$row->status_id = $post['status_id'];
                    $row->data_edit = date("Y-m-d H:i:s");
                    $row->user_edit = $this->login->user->id;
                    
                    $row->save();
                    $this->messenger->addMessage("Registro alterado com sucesso!","message");
                    $this->_redirect('admin/'.$this->section.'/edit/'.$row->alias.'/');
                } catch(Exception $e) {
                    $erro = strstr($e->getMessage(),"Duplicate") ?
                            "Já existe um registro semelhante, escolha outro nome." :
                            $e->getMessage();
                    $this->messenger->addMessage($erro,'erro');
                }
            }
        }
        
        $data = Is_Array::utf8DbRow($row);
        $data->body = nl2br($data->body);
        //Is_Var::dump((array)$data);
        $form->populate((array)$data);
        
        $this->view->form = $form;
        $this->view->action = 'edit';
        
        $this->fotosAction();
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            $fotos->delete("id=".(int)$id);
            Is_File::del($this->img_path.'/'.$foto->path);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('cases_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if(isset($this->pagina_id)){
            $select->where('case_id = ?',$this->pagina_id);
        }
        
        $_fotos = $select->query()->fetchAll();
        
        array_walk($_fotos,'Func::_arrayToObject');
        
        $fotos = array(); $fotos2 = array();
        
        for($i=0;$i<sizeof($_fotos);$i++){
            in_array($_fotos[$i]->flag,array('g','2')) ?
                $fotos2[] = $_fotos[$i] :
                $fotos[] = $_fotos[$i];
        }
        //_d(array($fotos,$fotos2));
        $this->view->fotos = $fotos;
        $this->view->fotos2 = $fotos2;
    }
    
    public function uploadAction()
    {
        //echo $this->img_path;exit();
        
        if(!$this->_request->isPost()){
            return array('error'=>'Método não permitido.');
        }
        
        $file = $_FILES['file'];
        $isFile = $this->_hasParam('arquivo');
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Count', false, 1);
        
        if($isFile){
            $upload->addFilter('Rename',$this->file_path.'/'.$rename);
        } else {
            $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
                   ->addValidator('Size', false, array('max' => '2048kB'))
                   ->addFilter('Rename',$this->img_path.'/'.$rename);
        }
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até 2MB.');
        }
        
        try {
            $upload->receive();
            
            //$fotos
            $table  = $isFile ? new Application_Model_Db_Arquivos() : new Application_Model_Db_Fotos();
            //$paginas_fotos
            $table2 = $isFile ? new Application_Model_Db_CasesArquivos() : new Application_Model_Db_CasesFotos();
            $type   = $isFile ? "arquivo" : "foto";
            $pagina_id = $this->_getParam('id');
            
            //$data_fotos
            $data_insert = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s"),
                'flag'     => $this->_hasParam('flag') ?
                              $this->_getParam('flag') : null
            );
            
            if(!$insert_id = $table->insert($data_insert)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            
            $table2->insert(array(
                $type."_id" => $insert_id,
                "case_id" => $pagina_id
            ));
            
            return array("name"=>$rename,"id"=>$insert_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function saveDescricaoAction()
    {
        if(!$this->_hasParam('pagina_id') ||
           !$this->_hasParam('foto_id') ||
           !$this->_hasParam('descricao')) {
            return array('error'=>'Acesso negado');
        }
        
        $f = new Application_Model_Db_Fotos();
        
        try{
            $f->update(
                array('descricao'=>$this->_getParam('descricao')),
                'id='.$this->_getParam('foto_id')
            );
            
            return array('msg'=>'Salvo.');
        } catch(Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }
}

