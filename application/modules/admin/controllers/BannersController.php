<?php

class Admin_BannersController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "BANNERS";
        $this->view->section = $this->section = "banners";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
        
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/..".FILE_PATH."/".$this->section;
        
        $this->banner_home = new Application_Model_Db_BannerHome();
        $this->fotos = new Application_Model_Db_Fotos();
        $this->table = null;
    }

    public function indexAction()
    {
        if(!$this->_hasParam('table')) $this->_redirect('admin');
        
        $this->table = $this->{$this->_getParam('table')};
        $p = array('/banners?_/','/_/');
        $r = array('','-');
        $this->view->name = preg_replace($p,$r,$this->_getParam('table'));
        $this->view->titulo = $this->view->titulo.' '.$this->view->name;
        
        /* paginação */
        $records_per_page   = 20;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $where = $post['search-by']." like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%'";
            $rows = $this->table->fetchAll($where,$post['search-by'],$limit,$offset);
            
            $total = $this->view->total = $this->table->count($where);
        } else {
            $rows = $this->table->fetchAll(null,"id desc",$limit,$offset);
            $total = $this->view->total = $this->table->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function homeAction()
    {
        $this->_forward('index','banners','admin',array('table'=>'banner_home'));
    }
    
    public function homeNewAction()
    {
        $this->view->titulo = '<a href="'.$this->_url.'home">'.
                              $this->view->titulo.' HOME</a> &rarr; '.
                              ($this->_hasParam('data')?"EDITAR":"NOVO");
        
        $table_name = 'banner_home'; $name = 'home';
        $this->view->table_name = $table_name;
        $this->view->name = $name;
        
        if($this->_hasParam('data')){
            $data = Is_Array::toObject($this->_getParam('data'));
            if((bool)$data->foto_id){
                $data->foto = $this->fotos->fetchRow('id='.$data->foto_id);
            }
            
            $this->view->id = $this->banner_id = $data->id;
            $this->view->data = $data;
            //$this->view->fotos = $this->fotosAction();
        } else {
            $this->view->data = Is_Array::toObject(array(
                'status_id'=>'1'
            ));
        }
        
        $this->view->table_meta = $this->banner_home->info('metadata');
    }
    
    public function homeSaveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url));
            return;
        }
        
        $table_name = 'banner_home'; $name = 'home';
        $id = (int)$this->_getParam("id");
        $row = $this->$table_name->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = array_map('utf8_decode',$this->_request->getParams());
            if(isset($data['nome'])) $data['alias'] = Is_Str::toUrl($this->_getParam('nome'));
            if($data['body']) $data['body']  = strip_tags($data['body'],'<b><a><i><u><br><ul><ol><li><img><p><div>');
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            $data['data_edit'] = date("Y-m-d H:i:s");
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->$table_name->update($data,'id='.$id) : $id = $this->$table_name->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $this->_redirect('admin/banners/'.$name.'-edit/'.$id);
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ? 'Já existe um banner com esta descrição. Por favor, escolha outra.' : $e->getMessage();
            $this->messenger->addMessage($error,'error');
            $this->_forward($name.'-new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        if(!$this->_hasParam('id') || !$this->_hasParam('name')){
            $this->_forward('denied','error','default',array('url'=>$this->_url));
            return;
        }
        
        $name = $this->_getParam('name'); $table_name = 'banner_'.$name;
        $id    = (int)$this->_getParam('id');
        $row   = $this->$table_name->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward($name.'-new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $name = $this->_getParam('name'); $table_name = 'banner_'.$name;
        $id = $this->_getParam("id");
        $row = $this->$table_name->fetchRow($id);
        
        try {
            if((bool)$row->foto_id){
                if($foto = $this->fotos->fetchRow('id='.$row->foto_id)){
                    Is_File::del($this->img_path.'/'.$foto->path);
                    Is_File::delDerived($this->img_path.'/'.$foto->path);
                    $this->fotos->delete("id=".(int)$row->foto_id);
                }
            }
            
            $this->$table_name->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function uploadAction()
    {
        //echo $this->img_path;exit();
        $max_size = '5120'; // '2048'
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/produtos/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size.'kb'))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.Is_File::formatBytes($max_size).'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $produtos_fotos = $this->{'banner_'.$this->_getParam('name')};
            $produto_id = $this->_getParam('id');
            
            $data_fotos = array(
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $produtos_fotos->update(array("foto_id"=>$foto_id),"id=".$produto_id);
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }

}

