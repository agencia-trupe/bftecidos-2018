<?php

class Admin_Form_Clientes extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/clientes/save')
             ->setAttrib('id','frm-noticias')
             ->setAttrib('name','frm-noticias');
        
        // elementos
        $this->addElement('text','titulo_pt',array('label'=>'Nome (pt)','class'=>'txt'));
        $this->addElement('text','titulo_en',array('label'=>'Nome (en)','class'=>'txt'));
        $this->addElement('hidden','alias');
        $this->addElement('hidden','pagina_id');
        // $this->addElement('select','perfil_id',array('label'=>'Perfil','class'=>'txt','multiOptions'=>perfilKeyValues()));
        // $this->addElement('select','lang',array('label'=>'Idioma','class'=>'txt','multiOptions'=>langKeyValues()));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        // $this->addElement('textarea','olho',array('label'=>'Olho','class'=>'txt'));
        $this->addElement('textarea','body_pt',array('label'=>'Descrição (pt)','class'=>'txt wysiwyg'));
        $this->addElement('textarea','body_en',array('label'=>'Descrição (en)','class'=>'txt wysiwyg'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        // $this->getElement('body')->setAttrib('rows',25)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo_pt')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

