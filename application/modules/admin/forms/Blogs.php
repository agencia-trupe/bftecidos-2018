<?php

class Admin_Form_Blogs extends ZendPlugin_Form
{
    public function init()
    {
        $users = new Application_Model_Db_Usuario();

        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/blogs/save')
             ->setAttrib('id','frm-noticias')
             ->setAttrib('name','frm-noticias');
        
        // elementos
        $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        // $this->addElement('text','data',array('label'=>'Data','class'=>'txt mask-date'));
        $this->addElement('hidden','alias');
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        $this->addElement('textarea','descricao',array('label'=>'Descrição','class'=>'txt'));
        $this->addElement('select','user_id',array(
            'label'=>'Vincular usuário',
            'class'=>'txt',
            'multiOptions'=>$users->getKeyValues('nome',true,'role in (3)')
        ));
        $this->addElement('select','moderacao_posts',array(
            'label'=>'Mod. de Posts',
            'class'=>'txt',
            'multiOptions'=>array(
                '0' => 'Não moderado',
                '1' => 'Pré moderado',
                '2' => 'Pós moderado',
            )
        ));
        $this->addElement('select','moderacao_comments',array(
            'label'=>'Mod. de Comentários',
            'class'=>'txt',
            'multiOptions'=>array(
                '0' => 'Não moderado',
                '1' => 'Pré moderado',
                '2' => 'Pós moderado',
            )
        ));
        $this->addElement('checkbox','require_login_posts',array('label'=>'Blog restrito'));
        $this->addElement('checkbox','require_login_comments',array('label'=>'Comentários restritos'));
        $this->addElement('checkbox','has_comments',array('label'=>'Comentários visíveis'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        $this->getElement('descricao')->setAttrib('rows',10)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

