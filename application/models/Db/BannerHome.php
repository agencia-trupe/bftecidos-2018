<?php

class Application_Model_Db_BannerHome extends ZendPlugin_Db_Table {
    protected $_name = "banner_home";
    
    protected $_dependentTables = array(
        'Application_Model_Db_Fotos',
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Fotos' => array(
            'columns' => 'foto_id',
            'refTableClass' => 'Application_Model_Db_Fotos',
            'refColumns'    => 'id'
        ),
    );
}
