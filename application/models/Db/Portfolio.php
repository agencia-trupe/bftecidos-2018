<?php

class Application_Model_Db_Portfolio extends ZendPlugin_Db_Table 
{
    protected $_name = "portfolio";
    protected $_foto_join_table = 'portfolio_fotos';
    protected $_foto_join_table_field = 'portfolio_id';
    protected $_foto_join_field = 'foto_id';
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_PortfolioFotos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_PortfolioFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PortfolioFotos',
            'refColumns'    => 'portfolio_id'
        )
    );
    
    /**
     * Retorna produto com suas imagens com base no alias se @alias for string ou id se @alias for numérico
     *
     * @param string|int $alias - valor do alias ou id do produto
     *
     * @return object|bool - objeto contendo o produto com suas imagens e categoria ou false se não for encontrado
     */
    public function getWithFotos($alias)
    {
        $column = is_numeric($alias) ? 'id' : 'alias';
        if(!$produto = $this->fetchRow($column.'="'.$alias.'"')){
            return false;
        }
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_PortfolioFotos')){
            foreach($produto_fotos as $produto_foto){
                $fotos[] = $produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current();
            }
        }
        
        $object = Is_Array::utf8DbRow($produto);
        $object->fotos = $fotos;
        //_d($object);
        return $object;
    }
    
    /**
     * Retorna as fotos do produto
     *
     * @param int $id - id do produto
     * @param string $id - string where adicional para fotos (f)
     *
     * @return array - rowset com fotos do produto
     */
    public function getFotos1($id,$w=null)
    {
        if(!$produto = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        
        // $fotos = array();
        // if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_PortfolioFotos')){
        //     foreach($produto_fotos as $produto_foto){
        //         $fotos[] = ($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
        //     }
        // }

        $fotos = $this->q(
            'select f.* from portfolio_fotos pf '.
            'left join fotos f on f.id = pf.foto_id '.
            'where pf.portfolio_id = "'.$id.'" '.
            ($w ? 'and '.$w.' ' : '').
            'order by f.ordem '.
            'limit 1000'
        );
        
        return $fotos;
    }
    
    /**
     * Retorna as fotos do produto
     *
     * @param array $rows - rowset de projetos
     *
     * @return array - rowset com fotos
     */
    public function getFotos(&$rows)
    {
        $pids = array(); // projetos id
        
        if(!is_array($rows)){
            $pids[] = $rows->id;
        } else {
            foreach($rows as &$row){ // pegando id's para fotos            
                $pids[] = $row->id;
            }
        }
        
        $fotos = $this->q( // pegando fotos
            'select f.*, pf.portfolio_id from portfolio_fotos pf '.
            'left join fotos f on f.id = pf.foto_id '.
            'where pf.portfolio_id in ('.(count($pids) ? implode(',',$pids) : '0').') '.
            'order by f.ordem'
        );
        
        if((bool)$fotos){ // assimilando fotos aos projetos
            if(!is_array($rows)){
                $rows->fotos = $fotos;
                $rows->capa = null;

                foreach($fotos as $foto){
                    if($rows->capa_id == $foto->id) $rows->capa = $foto;
                }
            } else {
                foreach($rows as &$row){
                    $row->capa = null;
                    $row->fotos = array();
                    
                    foreach($fotos as $foto){
                        if($row->capa_id == $foto->id) $row->capa = $foto;
                        
                        $row->fotos[] = $foto;
                    }
                }
            }
        }

        return $rows;
    }
    
    /**
     * Retorna quantidade total
     * 
     * @param string $where - string de seleção where, padrão NULL
     *
     * @return int
     */
    public function count($where=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from($this->_name,array('count(*) as cnt'));
        $count = $where ? $select->where($where)->query()->fetchAll() : $select->query()->fetchAll();
        return $count[0]['cnt'];
    }

    public function getLastProjects($count=null,$where=null)
    {
        return $this->q(
            // 'select p.titulo_pt, p.titulo_en, p.alias, p.cliente, p.thumbnail from portfolio p '.
            'select p.id, p.titulo_pt, p.titulo_en, p.alias, p.cliente, f.path as thumbnail from portfolio p '.
            'left join portfolio_fotos pf on pf.portfolio_id = p.id '.
            'left join fotos f on f.id = pf.foto_id '.
            'where p.status_id = 1 '.
            'and f.flag = 1 '.
            ($where!==null ? 'and '.$where.' ' : ' ').
            'group by p.id '.
            'order by p.ordem '.
            // 'order by p.destaque desc, p.ano desc, p.data_edit desc '.
            ($count!==null ? 'limit '.$count.' ' : ' ')
        );
    }

    public function getLastOrdemFotos($pid,$where=null)
    {
        $q = 'select max(f.ordem) as ord from fotos f ';
        $q.= 'left join portfolio_fotos pf on pf.foto_id = f.id ';
        $q.= 'where pf.portfolio_id = "'.$pid.'" ';
        if($where) $q.= 'and ('.$where.') ';
        $row = $this->q($q);
        return (int)$row[0]->ord;
    }

    public function getNextOrdemFotos($pid,$where=null)
    {
        return $this->getLastOrdemFotos($pid,$where) + 1;
    }
}