<?php

class Application_Model_Db_Clientes extends ZendPlugin_Db_Table
{
    protected $_name = "clientes";
    protected $_login_column = "email";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Pedidos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pedidos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Pedidos',
            'refColumns'    => 'cliente_id'
        )
    );
    
    // gets
    public function findByLoginAtivo($email,$password)
    {
        return $this->fetchRow($this->_login_column.' = "'.$email.'" and senha = "'.md5($password).'" and status_id = 1');
    }
    
    public function findByEmail($email)
    {
        return $this->fetchRow('email = "'.$email.'"');
    }
    
    /**
     * Retorna quantidade total
     * 
     * @param string $where - string de seleção where, padrão NULL
     *
     * @return int
     */
    public function count($where=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from($this->_name,array('count(*) as cnt'));
        $count = $where ? $select->where($where)->query()->fetchAll() : $select->query()->fetchAll();
        return $count[0]['cnt'];
    }
    
    public function getFotos($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('clientes_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('cliente_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
    
    public function getWithFotos($where=null,$order=null,$limit=null)
    {
    	return $this->q(
            'select c.*,f.path from clientes c '.
            'left join clientes_fotos cf on cf.cliente_id = c.id '.
            'left join fotos f on f.id = cf.foto_id '.
            ($where ? 'where '.$where.' ' : '').
            ($order ? 'order by '.$order.' ' : '').
            ($limit ? 'limit '.$limit.' ' : '')
        );
    }
}