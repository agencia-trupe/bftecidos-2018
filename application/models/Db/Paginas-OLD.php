<?php
/**
 * Modelo da tabela de usuarios
 */
class Application_Model_Db_Paginas extends Zend_Db_Table 
{
    protected $_name = "paginas";
    
    protected $_dependentTables = array('Aluno_Model_Pessoa');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pessoa' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Pessoa',
            'refColumns'    => 'usuario_id'
        )
    );

    public function getFotos($id=null)
    {
    	$select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('pagina_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    public function getFotosFixas($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos_fixas as tf')
            ->order('tf.id asc');
        
        if($id) $select->where('pagina_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return count($fotos) ? $fotos[0] : null;
    }

    public function getEtapas($id=null,$withFotos=true)
    {
    	$table = new Application_Model_Db_Etapas();
        $where = 'pagina_id in ('.$id.')';
        
        $rows = $table->fetchAll($where,'ordem');
        $rows = Is_Array::utf8DbResult($rows);

        if($withFotos){
            $ids = array();

            foreach ($rows as $row) $ids[] = $row->id;

            $_fotos = $table->q(
                'select ef.etapa_id, f.* from etapas_fotos ef '.
                'left join fotos f on f.id = ef.foto_id '.
                'where ef.etapa_id in('.(count($ids) ? implode(',',$ids) : '0').') '
            );

            $fotos = array();
            foreach($_fotos as $f){
                if(!isset($fotos[$f->etapa_id])) $fotos[$f->etapa_id] = array();
                $fotos[$f->etapa_id][] = $f;
            }

            foreach ($rows as &$row){
                $row->fotos = isset($fotos[$row->id]) ? $fotos[$row->id] : array();
            }
        }

        return $rows;
    }
}