<?php

class Application_Model_Db_MateriasFotos extends Zend_Db_Table
{
    protected $_name = "materias_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Materias');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Materias' => array(
            'columns' => 'materia_id',
            'refTableClass' => 'Application_Model_Db_Materias',
            'refColumns'    => 'id'
        )
    );
}
