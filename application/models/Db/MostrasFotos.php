<?php

class Application_Model_Db_MostrasFotos extends Zend_Db_Table
{
    protected $_name = "mostras_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array(
        'Application_Model_Db_Mostras',
        'Application_Model_Db_Fotos'
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Mostras' => array(
            'columns' => 'mostra_id',
            'refTableClass' => 'Application_Model_Db_Mostras',
            'refColumns'    => 'id'
        ),
        'Application_Model_Db_Fotos' => array(
            'columns' => 'foto_id',
            'refTableClass' => 'Application_Model_Db_Fotos',
            'refColumns'    => 'id'
        )
    );
}
