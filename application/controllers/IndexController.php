<?php

class IndexController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        // models
        $this->paginas = new Application_Model_Db_PaginaHome();
        // $this->projetos = new Application_Model_Db_Portfolio();
    }

    public function indexAction()
    {
        $pagina = Is_Array::utf8DbRow(
            $this->paginas->fetchRow('id = 1')
        );

        $pagina->fotos = $this->paginas->getFotos($pagina->id);
        
        $this->view->pagina = $pagina;
        // $this->view->titulo = $pagina->{'titulo_'.$this->view->lang};

        // $this->view->projetos = $this->projetos->getLastProjects(3);
    }
    
    public function mailAction()
    {
        try{
            Trupe_CynthiaPimentel_Mail::send('patrick@trupe.net','Icko','Confirmação','Confirmar leitura do texto');
            //Trupe_CynthiaPimentel_Mail::send('icko.s@hotmail.com','Icko','Confirmação','Confirmar leitura do texto');
            //Trupe_CynthiaPimentel_Mail::send('patrick@ickostudios.com','Icko','Confirmação','Confirmar leitura do texto');
            echo "ok";
        } catch(Exception $e){
            echo $e->getMessage().' ('.APPLICATION_ENV.')';
        }
        exit();
    }
    
    public function newsletterAction()
    {
        if($this->_request->isPost()){
            $post = $this->_request->getPost();
            $post = $post['newsletter'];
            $validator = new Zend_Validate_EmailAddress();
            
            if(trim($post['nome']) == '' ||
               trim($post['nome']) == 'nome' ||
               trim($post['telefone']) == '' ||
               trim($post['telefone']) == 'telefone' ||
               trim($post['telefone']) == '(__)____-____' ||
               strlen(trim(Is_Str::removeCaracteres($post['telefone']))) != 10 ||
               !$validator->isValid($post['email'])){
                return array("error"=>1,"message"=>"* Preencha todos os campos");
            } else {
                try {
                    $promocoes = new Application_Model_Db_Promocoes();
                    $promocoes_row = $promocoes->fetchRow($promocoes->select()->order('id desc'));
                    
                    $data = array_map('utf8_decode',$post);
                    $data['data_cad'] = date('Y-m-d H:i:s');
                    $data['telefone'] = Is_Str::removeCaracteres($data['telefone']);
                    $data['promocao_id'] = $promocoes_row ? $promocoes_row->id : null;
                    //unset($data['submit']);
                    $table = new Application_Model_Db_Mailling();
                    $table->insert($data);
                    
                    $html = '<h1 style="font-size:14px">Novo cadastro</h1><p style="font-size:11px">'.
                        '<b>Nome:</b> '.$post['nome'].'<br />'.
                        '<b>E-mail:</b> <a href="mailto:'.$post['email'].'">'.$post['email'].'</a><br />'.
                        '<b>Telefone:</b> <a href="mailto:'.$post['telefone'].'">'.$post['telefone'].'</a><br />'.
                        '</p>';
                    
                    $html2 = '<h1 style="font-size:14px">Obrigado!</h1><p style="font-size:11px">'.
                        // 'Obrigado por se cadastrar na promoção "'.($promocoes_row?utf8_encode($promocoes_row->titulo):'').'".<br/>'.
                        // 'Ao realizarmos o sorteio, informaremos so você foi a ganhadora!<br/><br/>'.
                        // 'Confira abaixo seus dados cadastrais, e boa sorte! <br /><br />'.
                        '<b>Nome:</b> '.$post['nome'].'<br />'.
                        '<b>E-mail:</b> <a href="mailto:'.$post['email'].'">'.$post['email'].'</a><br />'.
                        '<b>Telefone:</b> <a href="mailto:'.$post['telefone'].'">'.$post['telefone'].'</a><br />'.
                        '</p>';
                    
                    Trupe_CynthiaPimentel_Mail::sendWithReply(
                        $post['email'],
                        $post['nome'],
                        'Novo cadastro',
                        $html
                    );
                    
                    Trupe_CynthiaPimentel_Mail::send(
                        $post['email'],
                        $post['nome'],
                        'Cadastro efetuado com sucesso',
                        $html2
                    );
                    
                    return array("message"=>"Cadastrado! Obrigado pela participação.");
                    $form->reset();
                } catch(Exception $e){
                    $msg = strstr($e->getMessage(),'uplicate') ? '* Você já se cadastrou' : '* Erro ao enviar';
                    return array("error"=>1,"message"=>$msg);
                }
            }
        }
    }

    public function phpinfoAction()
    {
        _d(phpinfo());
    }

    public function resizeAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '512M');

        $folder = $this->_hasParam('f') ? '/'.$this->_getParam('f') : '';
        $path = APPLICATION_PATH.'/..'.IMG_PATH.$folder;
        $q = $this->_hasParam('q') ? $this->_getParam('q') : 70;
        $last = '111111111111111111';
        $sess = SITE_NAME.'_rimgs'.$folder;
        $counter = 0;
        if(!isset($_SESSION[$sess])) $_SESSION[$sess] = array();

        foreach(glob($path.'/*.[jJ][pP][gG]') as $img){
        // foreach(glob($path.'/*.[pP][nN][gG]') as $img){
            try {
                if(file_exists($img)) {
                    $im = end(explode('/',$img));
                    if(!strstr($im,'_') 
                        && !in_array($img,$_SESSION[$sess])
                        && filesize($img)>1000000
                        // && reset(explode('.',$im)) > $last
                    ){
                        // echo 'reducing '.$img.'<br>';
                        // echo Is_File::formatBytes(filesize($img));
                        // echo ' ('.filesize($img).')<br>';
                        
                        $thumb = Php_Thumb_Factory::create($img);
                        $thumb->resize(2000,2000);
                        $thumb->save($img);
                        Image::setQuality($img,$img,$q);

                        $_SESSION[$sess][] = $img;
                        $counter++;
                    }
                } else {
                    echo '<b>NO EXIST</b> '.$img.'<br>';
                }
            } catch(Exception $e){
                $err = '<br><br>---------------------<br>';
                $err.= 'ERROR ON IMAGE: '.$img.'<br>'.$e->getMessage();
                $err = '<br>---------------------<br><br>';
                exit($err);
            }
        }

        echo '<br><br>OK ('.$counter.' images reduced - '.count($_SESSION[$sess]).' in session)<br><br>';
        foreach($_SESSION[$sess] as $row) echo end(explode('/',$row)).'<br>';
        exit();
    }
    
}