<?php

class ColecaoController extends ZendPlugin_Controller_Action
{
	protected $_require_db = array(
		'table' => 'Portfolio',
		'marcas' => 'Marcas',
	);

    public function init()
    {
        $menu = $this->marcas->getMenu();
        $this->view->menu_marcas = $menu;
    }

    public function indexAction()
    {
    	$alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
    	if(!$alias) return $this->_redirect('colecoes');

    	$id = end(explode('-', $alias));
    	$where = 't1.id = "'.$id.'" or t1.alias = "'.$alias.'" ';

        $rows = $this->table->fetchAllWithPhoto($where,'t1.titulo_'.$this->view->lang,null,null,array(
            'join' => 'left join marcas as m on m.id = t1.marca_id',
            'extra_fields' => 'm.titulo_pt as marca_pt, m.titulo_en as marca_en',
            'group' => 't1.id'
        ));
    	
    	if(!$rows) return $this->_redirect('colecoes');
    	$row = $rows[0];
    	$fotos = $this->table->getFotos1($row->id,'f.flag not in (1)');
    	$row->fotos = $fotos;

    	$marca = _utfRow($this->marcas->get($row->marca_id));
        
        $this->view->row = $row;
        $this->view->fotos = $fotos;
        $this->view->marca = $marca;
    }


}

