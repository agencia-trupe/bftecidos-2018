<?php

class MostrasController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        $this->mostras = new Application_Model_Db_Mostras();
        $this->fotos = new Application_Model_Db_Fotos();
    }

    public function indexAction()
    {
        $limit = 6;
        $categoria = null;
        $paginacao = $this->pagination($limit,10,$this->mostras->count('status_id = 1'.(($categoria)?' and categoria_id = "'.$categoria.'"':'')));
        // _d($paginacao);
        
        $this->view->rows = $this->mostras->getLastProjects(
            $paginacao->offset.','.$limit,
            (($categoria)?'categoria_id = "'.$categoria.'"':null),
            'p.ordem'
        );
        if($this->_hasParam('dump')) _d($this->view->rows);

        $this->view->pagination = $paginacao;
        $this->view->categoria = $categoria;
    }


}

