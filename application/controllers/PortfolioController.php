<?php

class PortfolioController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->portfolio = new Application_Model_Db_Portfolio();
        $this->fotos = new Application_Model_Db_Fotos();
    }

    public function indexAction()
    {
        $rows = $this->portfolio->fetchAll('status_id=1',"data_edit desc");
        
        if(count($rows)){
            $rows = Is_Array::utf8DbResult($rows);
            
            foreach($rows as &$row){                
                $row->capa = (bool)$row->capa_id ?
                    Is_Array::utf8DbRow($this->fotos->fetchRow('id='.$row->capa_id)):
                    null;
                
                $row->fotos = $this->portfolio->getFotos($row->id);
            }
        }
        
        $this->view->rows = $rows;
    }

}