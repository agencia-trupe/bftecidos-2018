<?php

class ProjetosController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->portfolio = new Application_Model_Db_Portfolio();
        $this->categorias = new Application_Model_Db_CategoriasPortfolio();
        $this->fotos = new Application_Model_Db_Fotos();

        $this->view->categorias = Is_Array::utf8DbResult(
            $this->categorias->fetchAll(null,'ordem')
        );
    }

    public function indexAction()
    {
        /*$rows = $this->portfolio->fetchAll('status_id=1',array('ano desc','data_edit desc'));
        
        if(count($rows)){
            $rows = Is_Array::utf8DbResult($rows);
            $rows = $this->portfolio->getFotos($rows);
        }
        
        $this->view->rows = $rows;*/

        // paginacao
        $limit = 6;
        $categoria = $this->_hasParam('categoria') ? 
                     $this->categorias->getIdFromAlias($this->_getParam('categoria')) : 
                     0;
        $paginacao = $this->pagination($limit,10,$this->portfolio->count('status_id = 1'.(($categoria)?' and categoria_id = "'.$categoria.'"':'')));
        // _d($paginacao);
        
        $this->view->rows = $this->portfolio->getLastProjects(
            $paginacao->offset.','.$limit,
            (($categoria)?'categoria_id = "'.$categoria.'"':null)
        );

        $this->view->pagination = $paginacao;
        $this->view->categoria = $categoria;
    }
    
}