<?php

class CasesController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->cases = new Application_Model_Db_Cases();
        $this->fotos = new Application_Model_Db_Fotos();
    }

    public function indexAction()
    {
        $c = $this->cases->fetchRow('parent_id=0 and status_id=1','ordem');
        
        if($c) $this->_redirect('cases/'.$c->alias);
        
        $this->_forward('not-found','error','default',array('url'=>URL,'msg'=>'Nenhum projeto disponível no momento'));
    }
    
    public function caseAction()
    {
        if(!$this->_hasParam('alias')){
            $this->_forward('not-found','error','default',array('url'=>URL));
            return;
        }
        
        $alias = $this->view->alias = $this->_getParam('alias');
        $alias2 = $this->view->alias2 = $this->_getParam('alias2');
        $row = Is_Array::utf8DbRow($this->cases->fetchRow('alias="'.$alias.'"'));
        $row2 = $this->_hasParam('alias2') ?
                Is_Array::utf8DbRow($this->cases->fetchRow('alias="'.$alias2.'"')) :
                null;
        
        if(!$row){
            $this->_forward('not-found','error','default',array('url'=>URL));
            return;
        }
        
        $row->children = Is_Array::utf8DbResult($this->cases->getChildren($row->id));
        
        $fotos = $this->fotos->fetchJoin(
            'cases_fotos as pf',
            'pf.case_id='.($row2 ? $row2->id : $row->id)
        );
        
        $fotos1 = array(); $fotos2 = array();
        
        foreach($fotos as $foto){
            ($foto->flag=='g' || $foto->flag=='2') ?
                $fotos2[] = $foto :
                $fotos1[] = $foto;
        }
        
        $this->view->case = $row;
        $this->view->case2 = $row2;
        $this->view->fotos = $fotos1;
        $this->view->fotos2 = $fotos2;
        $this->view->titulo = ($row2 ? $row2->titulo.' - ' :'').
                               $row->titulo.' - Cases';
    }

}