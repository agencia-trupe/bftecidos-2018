<?php

class MidiaController extends ZendPlugin_Controller_Action
{

    public function init()
    {

    }

    public function indexAction()
    {
        $this->_redirect('midia/clipping');
    }

    public function clippingAction()
    {
        $this->clippings = new Application_Model_Db_Clippings();

        $limit = 10;
        $categoria = null;
        $paginacao = $this->pagination($limit,10,$this->clippings->count('status_id = 1'.(($categoria)?' and categoria_id = "'.$categoria.'"':'')));
        // _d($paginacao);
        
        $rows = $this->clippings->getLastProjects(
            $paginacao->offset.','.$limit,
            (($categoria)?'categoria_id = "'.$categoria.'"':null)
        );

        if((bool)$rows) $rows = $this->clippings->getFotos($rows);

        $this->view->rows = $rows;
        $this->view->pagination = $paginacao;
        $this->view->categoria = $categoria;
        // _d($this->view->rows);
    }

    public function materiasAction()
    {
        $this->materias = new Application_Model_Db_Materias();

        $limit = 6;
        $paginacao = $this->pagination($limit,10,$this->materias->count('status_id = 1'));
        // _d($paginacao);
        
        $rows = Is_Array::utf8DbResult($this->materias->fetchAll(
            'status_id = 1',
            'data_cad desc',
            $limit, $paginacao->offset
        ));

        $this->view->rows = $rows;
        $this->view->pagination = $paginacao;
        // _d($this->view->rows);
    }

    public function videosAction()
    {
        $this->videos = new Application_Model_Db_Videos();

        $limit = 6;
        $paginacao = $this->pagination($limit,10,$this->videos->count('status_id = 1'));
        // _d($paginacao);
        
        $rows = Is_Array::utf8DbResult($this->videos->fetchAll(
            'status_id = 1',
            'data_cad desc',
            $limit, $paginacao->offset
        ));

        $this->view->rows = $rows;
        $this->view->pagination = $paginacao;
        // _d($this->view->rows);
    }


}

