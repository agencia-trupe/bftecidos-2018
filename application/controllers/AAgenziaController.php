<?php

class AAgenziaController extends ZendPlugin_Controller_Ajax 
{

    public function init()
    {
        $this->paginas = new Application_Model_Db_Paginas();
        $this->paginas_fotos = new Application_Model_Db_PaginasFotos();
        $this->fotos = new Application_Model_Db_Fotos();
    }

    public function indexAction()
    {
        $pagina = Is_Array::utf8DbRow($this->paginas->fetchRow('alias="a-agenzia"'));
        
        $pagina->fotos = $this->fotos->fetchJoin(
            'paginas_fotos as pf',
            'pf.pagina_id='.$pagina->id
        );
        
        $this->view->pagina = $pagina;
    }
    
}