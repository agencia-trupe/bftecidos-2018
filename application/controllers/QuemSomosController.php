<?php

class QuemSomosController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        $this->paginas = new Application_Model_Db_Paginas();
        $this->clientes = new Application_Model_Db_Clientes();
    }

    public function indexAction()
    {
        $pagina = Is_Array::utf8DbRow(
            $this->paginas->fetchRow('alias="quem-somos"')
        );

        $pagina->fotos = $this->paginas->getFotosFixas($pagina->id);
        
        $titulo = 'titulo_'.$this->view->lang;
        $this->view->pagina = $pagina;
        $this->view->titulo = $pagina->{$titulo};
        $this->view->grupo  = $this->clientes->getWithFotos('status_id = 1');
    }
    
}