<?php

class EmpresaController extends ZendPlugin_Controller_Action
{
	protected $_require_db = array(
		'paginas' => 'Paginas',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $pagina = ($this->paginas->getPagina(6));
        $this->view->pagina = $pagina;
    }


}

