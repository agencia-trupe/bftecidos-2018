<?php

class MarcasController extends ZendPlugin_Controller_Action
{
	protected $_require_db = array(
        'table' => 'Marcas',
		// 'paginas' => 'Paginas',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $rows = $this->table->q(
        	'select m.* '.
        		', f1.path as foto_path'.
        		', f2.path as foto_path2'.
                ', f3.path as foto_path3 '.
        		', f4.path as foto_path4 '.
        	'from marcas m '.
        	'left join fotos f1 on f1.id = m.foto_id '.
	        	'left join fotos f2 on f2.id = m.foto2_id '.
                'left join fotos f3 on f3.id = m.foto3_id '.
	        	'left join fotos f4 on f4.id = m.foto4_id '.
        	'where m.status_id = 1 '.
        	'order by m.ordem '.
        	'limit 1000'
        );
        $this->view->rows = $rows;

        // $pagina = _utfRow($this->paginas->get(8));
        $pagina = $this->table->s1('paginas','*','id=8');
        $this->view->pagina = $pagina;
    }


}

