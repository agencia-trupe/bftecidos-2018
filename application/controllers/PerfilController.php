<?php

class PerfilController extends Zend_Controller_Action
{

    public function init()
    {
        $this->paginas = new Application_Model_Db_Paginas();
    }

    public function indexAction()
    {
        $pagina = Is_Array::utf8DbRow(
            $this->paginas->fetchRow('alias = "perfil"')
        );

        $pagina->fotos = $this->paginas->getFotos($pagina->id);
        
        $this->view->pagina = $pagina;
    }


}

