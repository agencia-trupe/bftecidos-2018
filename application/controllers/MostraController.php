<?php

class MostraController extends Zend_Controller_Action
{

    public function init()
    {
        $this->mostras = new Application_Model_Db_Mostras();
        $this->fotos = new Application_Model_Db_Fotos();
    }

    public function indexAction()
    {
        $alias = $this->view->alias = addslashes($this->_getParam('alias'));

        $row = Is_Array::utf8DbRow(
        	$this->mostras->fetchRow('alias = "'.$alias.'"')
    	);

    	if(!$row) return $this->_redirect('projetos');
        
        $row = $this->mostras->getFotos($row);
        $this->view->row = $row;
    }


}

