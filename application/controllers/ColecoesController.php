<?php

class ColecoesController extends ZendPlugin_Controller_Action
{
	protected $_require_db = array(
		'table' => 'Portfolio',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $where = 't1.status_id=1 ';
        $alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
        if($alias) {
            $m = $this->table->s1('marcas','*','alias = "'.$alias.'"');
            if($m){
                // $row = $this->table->fetchRow('marca_id = "'.$m->id.'"','titulo_'.$this->view->lang);
                // if($row) return $this->_redirect('colecao/'.$row->alias.'-'.$row->id);
                $where.= 'and m.id = "'.$m->id.'" ';
            }
        }

        $rows = $this->table->fetchAllWithPhoto($where,'t1.ordem',null,null,array(
            'join' => 'left join marcas as m on m.id = t1.marca_id',
            'extra_fields' => 'm.titulo_pt as marca_pt, m.titulo_en as marca_en',
            'group' => 't1.id'
        ));
        $this->view->rows = $rows;
    }


}

